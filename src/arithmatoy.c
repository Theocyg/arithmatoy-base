#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
	if (VERBOSE) {
		fprintf(stderr, "add: entering function\n");
	}
	size_t len_lhs, len_rhs;
	len_lhs = strlen(lhs);
	len_rhs = strlen(rhs);
	size_t max_len = (len_lhs > len_rhs ? len_lhs : len_rhs);  // largest number
	char* end_num = calloc(
		max_len
		+ 1  // overflow (9+9 b10 = 18)
		+ 1  // NULL byte string terminator
		,
		sizeof(char)
	);

	int carry = 0;
	for (size_t i = 0; i < max_len; i += 1) {
		int lhs_digit = (i < len_lhs ? get_digit_value(lhs[len_lhs-1-i]) : 0);
		int rhs_digit = (i < len_rhs ? get_digit_value(rhs[len_rhs-1-i]) : 0);
		int sum = lhs_digit + rhs_digit + carry;
		carry = sum / base;
		end_num[max_len - i] = to_digit(sum % base);
	}
	if (carry > 0) {
		end_num[0] = to_digit(carry);
	} else {
		memmove(end_num, end_num + 1, max_len + 1);
	}

	return end_num;
	// Fill the function, the goal is to compute lhs + rhs
	// You should allocate a new char* large enough to store the result as a
	// string Implement the algorithm Return the result
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
	if (VERBOSE) {
		fprintf(stderr, "sub: entering function\n");
	}
	size_t len_lhs, len_rhs;
	len_lhs = strlen(lhs);
	len_rhs = strlen(rhs);
	char* end_num = calloc(
		len_lhs  // max length is that of lhs
		+ 1  // NULL byte string terminator
		,
		sizeof(char)
	);

	int borrow = 0;
	for (size_t i = 0; i < len_lhs; i += 1) {
		int lhs_digit = get_digit_value(lhs[len_lhs-1-i]);
		int rhs_digit = (i < len_rhs ? get_digit_value(rhs[len_rhs-1-i]) : 0);
		int diff = lhs_digit - rhs_digit - borrow;
		if (diff < 0) {
			diff += base;
			borrow = 1;
		} else {
			borrow = 0;
		}
		end_num[len_lhs - 1 - i] = to_digit(diff);
	}

	return drop_leading_zeros(end_num);
	// Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
	// You should allocate a new char* large enough to store the result as a
	// string Implement the algorithm Return the result
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
	if (VERBOSE) {
		fprintf(stderr, "mul: entering function\n");
	}
	size_t len_lhs, len_rhs;
	len_lhs = strlen(lhs);
	len_rhs = strlen(rhs);
	char* end_num = calloc(
		len_lhs + len_rhs
		+ 1  // NULL byte string terminator
		,
		sizeof(char)
	);

	for (size_t i = 0; i < len_lhs; i += 1) {
		int carry = 0;
		int lhs_digit = get_digit_value(lhs[len_lhs-1-i]);
		for (size_t j = 0; j < len_rhs; j += 1) {
			int rhs_digit = get_digit_value(rhs[len_rhs-i-j]);
			int product =
				lhs_digit
				* rhs_digit
				+ get_digit_value(end_num[len_lhs+len_rhs-1-i-j])
				+ carry;
		}
		if (carry > 0) {
			end_num[len_lhs-1-i] = to_digit(carry);
		}
	}

	return drop_leading_zeros(end_num);
	// Fill the function, the goal is to compute lhs * rhs
	// You should allocate a new char* large enough to store the result as a
	// string Implement the algorithm Return the result
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
	// Convert a digit from get_all_digits() to its integer value
	if (digit >= '0' && digit <= '9') {
		return digit - '0';
	}
	if (digit >= 'a' && digit <= 'z') {
		return 10 + (digit - 'a');
	}
	return -1;
}

char to_digit(unsigned int value) {
	// Convert an integer value to a digit from get_all_digits()
	if (value >= ALL_DIGIT_COUNT) {
		debug_abort("Invalid value for to_digit()");
		return 0;
	}
	return get_all_digits()[value];
}

char *reverse(char *str) {
	// Reverse a string in place, return the pointer for convenience
	// Might be helpful if you fill your char* buffer from left to right
	const size_t length = strlen(str);
	const size_t bound = length / 2;
	for (size_t i = 0; i < bound; ++i) {
		char tmp = str[i];
		const size_t mirror = length - i - 1;
		str[i] = str[mirror];
		str[mirror] = tmp;
	}
	return str;
}

const char *drop_leading_zeros(const char *number) {
	// If the number has leading zeros, return a pointer past these zeros
	// Might be helpful to avoid computing a result with leading zeros
	if (*number == '\0') {
		return number;
	}
	while (*number == '0') {
		++number;
	}
	if (*number == '\0') {
		--number;
	}
	return number;
}

void debug_abort(const char *debug_msg) {
	// Print a message and exit
	fprintf(stderr, debug_msg);
	exit(EXIT_FAILURE);
}
